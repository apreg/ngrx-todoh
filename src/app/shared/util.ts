export default {

  appendToStorage(name, data) {
    if (name === null) return false;
    var old = localStorage.getItem(name);
    try {
      old = JSON.parse(old);
    } catch (e) {
      old = [];
    }
    localStorage.setItem(name, JSON.stringify(old.concat(data)));
    return true;
  },
// TODO make an interface for these maybe
  generateGuid() {
    const S4 = () => (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    return `${S4()}${S4()}-${S4()}-${S4()}-${S4()}-${S4()}${S4()}${S4()}`;
  }
}
