import {Routes} from '@ngrx/router';
import {LoginComponent} from '../login/login.component';
import {TodoComponent} from '../todo/todo.component';
import {LoginGuard} from './login.guard.ts';
import {TodoGuard} from "./todo.guard";


export const routes:Routes = [
  {
    path: '/',
    guards: [LoginGuard],
    component: LoginComponent
  },
  {
    path: '/todo',
    guards: [ TodoGuard ],
    component: TodoComponent
  }
]
