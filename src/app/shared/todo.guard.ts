import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/switchMapTo';
import 'rxjs/add/operator/switch';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Guard, Route, Router, TraversalCandidate, LocationChange } from '@ngrx/router';
import {AppState} from "./reducers";
import {getCurrentUserId} from "./reducers";
import { Store } from '@ngrx/store';
import * as Rx from 'rxjs/rx';

@Injectable()
export class TodoGuard implements Guard {

  constructor(private store:Store<AppState>, private router:Router) {
  }

  protectRoute(candidate:TraversalCandidate) {
    return this.store.let(getCurrentUserId())
      .take(1)
      .map(currentUserId => {
        if (currentUserId) {
          return true;
        } else {
          this.router.replace('/');
          return false;
        }
      });
  }
}
