import { provideStore } from '@ngrx/store';
import reducers from './reducers';

export default provideStore(reducers);
