import {AuthEffects, UserEffects} from '../login/shared'

export const EFFECTS = [
  AuthEffects, UserEffects
];
