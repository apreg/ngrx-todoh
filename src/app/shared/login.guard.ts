import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Guard, Route, Router, TraversalCandidate, LocationChange } from '@ngrx/router';
import {AppState} from "./reducers";
import {getCurrentUserId} from "./reducers";
import { Store } from '@ngrx/store';

@Injectable()
export class LoginGuard implements Guard {

  constructor(private store:Store<AppState>, private router:Router) {
  }

  protectRoute(candidate:TraversalCandidate) {
    return this.store.let(getCurrentUserId())
      .take(1)
      .map(currentUserId => {
        if (currentUserId) {
          this.router.replace('/todo');
          return false;
        } else {
          return true;
        }
      });
  }
}
