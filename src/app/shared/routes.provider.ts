import { provideRouter } from '@ngrx/router';
import {routes} from "./routes";
import { HashLocationStrategy } from '@angular/common';

export default provideRouter(routes, HashLocationStrategy);
