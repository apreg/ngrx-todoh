/**
 * combineReducers is another useful metareducer that takes a map of reducer
 * functions and creates a new reducer that stores the gathers the values
 * of each reducer and stores them using the reducer's key. Think of it
 * almost like a database, where every reducer is a table in the db.
 */
import { combineReducers } from '@ngrx/store';
import {localStorageSync} from 'ngrx-store-localstorage';
import { Observable } from 'rxjs/Observable';
import '@ngrx/core/add/operator/select';

/**
 * The compose function is one of our most handy tools. In basic terms, you give
 * it any number of functions and it returns a function. This new function
 * takes a value and chains it through every composed function, returning
 * the output.
 *
 * More: https://drboolean.gitbooks.io/mostly-adequate-guide/content/ch5.html
 */
import { compose } from '@ngrx/core/compose';

/**
 * storeLogger is a powerful metareducer that logs out each time we dispatch
 * an action.
 *
 * A metareducer wraps a reducer function and returns a new reducer function
 * with superpowers. They are handy for all sorts of tasks, including
 * logging, undo/redo, and more.
 */
import { storeLogger } from 'ngrx-store-logger';

/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single object.
 */
import todoReducer, * as fromTodo from '../todo/shared/todo.reducer';
import authReducer, * as fromAuth from '../login/shared/auth.reducer';
import userReducer, * as fromUser from "../login/shared/user.reducer";
import getTodosByUserId from "../todo/shared/todo.reducer";

/**
 * each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface AppState {
  todos: fromTodo.TodoState;
  auth: fromAuth.AuthState;
  users: fromUser.UserState;
}

/**
 * Because metareducers take a reducer function and return a new reducer,
 * we can use our compose helper to chain them together. Here we are
 * using combineReducers to make our top level reducer, and then
 * wrapping that in storeLogger. Remember that compose applies
 * the result from right to left.
 */
export default compose(storeLogger(), localStorageSync(['todos', 'auth', 'users'], true), combineReducers)({
  todos: todoReducer,
  auth: authReducer,
  users: userReducer
});

/**
 * A selector function is a map function factory. We pass it parameters and it
 * returns a function that maps from the larger state tree into a smaller
 * piece of state. This selector simply selects the `books` state.
 *
 * Selectors are used with the `let` operator. They take an input observable
 * and return a new observable. Here's how you would use this selector:
 *
 * ```ts
 * class MyComponent {
 * 	constructor(state$: Observable<AppState>) {
 * 	  this.booksState$ = state$.let(getBooksState());
 * 	}
 * }
 * ```
 */
export function getUserState() {
  return (state$:Observable<AppState>) => state$
    .select(s => s.users);
}

export function getTodoState() {
  return (state$:Observable<AppState>) => state$
    .select(s => s.todos);
}

export function getAuthState() {
  return (state$:Observable<AppState>) => state$
    .select(s => s.auth);
}

export function getUserById(id:string) {
  return compose(fromUser.getUserById(id), getUserState());
}

export function getCurrentUserId() {
  return compose(fromAuth.getCurrentUserId(), getAuthState());
}

export function getTodosOfUser(userId:string) {
  return compose(fromTodo.getTodosByUserId(userId), getTodoState());
}

export function getTodosOfCurrentUser() {
  return (state$:Observable<AppState>) => state$
    .let(getCurrentUserId())
    .switchMap(currentUserId => state$.let(getTodosOfUser(currentUserId)));
}

