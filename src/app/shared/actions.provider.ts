import {AuthActions} from '../login/shared/auth.action';
import {UserActions} from '../login/shared/user.action';

export const ACTIONS_PROVIDERS = [
  AuthActions, UserActions
];
