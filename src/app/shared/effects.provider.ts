import { runEffects } from '@ngrx/effects';
import {EFFECTS} from './effects';

export const EFFECT_PROVIDERS = runEffects(EFFECTS);
