import { Component } from '@angular/core';
import '../assets/style/styles.css';
import 'normalize.css';

import {AuthActions} from "./login/shared/auth.action";
import {AppState} from './shared/reducers';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import {getAuthState} from './shared/reducers';
import {Idle, DEFAULT_INTERRUPTSOURCES} from 'ng2-idle/core';

@Component({
  moduleId: module.id,
  selector: 'app',
  template: `<main class="app">
  <route-view></route-view>
</main>`,
  styles: [require('./app.component.css')]
})

export class App {

  constructor(private store:Store<AppState>, private idle:Idle, private authActions:AuthActions) {
    this.idle.setIdle(1);
    this.idle.setTimeout(4999);
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this.idle.onTimeout.subscribe(() => {
      this.store.dispatch(this.authActions.logout());
    });

    this.store.let(getAuthState()).subscribe(update => {
        if (update.currentUserId) {
          this.idle.watch();
        } else {
          this.idle.stop();
        }

      }
    );
  }
}
