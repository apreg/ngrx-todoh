import {Component} from '@angular/core'
import {User} from "./shared/user.model.ts";
import {ControlGroup, FormBuilder, Validators, Control, FORM_DIRECTIVES, CORE_DIRECTIVES} from "@angular/common";
import {OnInit} from '@angular/core';
import {AuthEffects} from "./shared/auth.effect";
import { Store } from '@ngrx/store';
import {AuthActions} from "./shared/auth.action";
import {AppState} from '../shared/reducers';
import { Observable } from 'rxjs/Observable';
import {getAuthState} from "../shared/reducers";
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'login',
  directives: [CORE_DIRECTIVES, FORM_DIRECTIVES],
  providers: [AuthEffects, AuthActions],
  styles: [require('./login.component.css')],
  template: `<div class="login">
  <div class="logo-container">
    <img src="assets/img/login-logo.png" class="login__logo">
  </div>
  <form [ngFormModel]="form" class="login__form" (ngSubmit)="login(form.value)">

    <span *ngIf="username.hasError('required') && username.touched">Username is required.</span>
    <span *ngIf="username.hasError('minlength') && username.touched">Username must be at least 3 characters long.</span>
    <input [ngClass]="{'inputError' : !username.valid && username.touched}" class="login-form__username"
           ngControl="username" type="text" placeholder="username">

    <span *ngIf="password.hasError('required') && password.touched">Password is required.</span>
    <span *ngIf="password.hasError('minlength') && password.touched">Password must be at least 3 characters long.</span>
    <input [class.inputError]="!password.valid && password.touched" class="login-form__password"
           [ngFormControl]="form.controls['password']"
           type="password" placeholder="password">
    <span>{{errorMsg$ | async}}</span>
    <button class="login-form__button" [disabled]="!form.valid" type="submit">login / register</button>
  </form>
</div>`
})
export class LoginComponent implements OnInit {

  errorMsg$:Observable<string>;
  form:ControlGroup;
  username;
  password;

  constructor(private fb:FormBuilder, private store:Store<AppState>, private authActions:AuthActions) {

  }

  ngOnInit() {
    this.username = new Control('asd', Validators.compose([
      Validators.required,
      Validators.minLength(3)
    ]));
    this.password = new Control('asd', Validators.compose([
      Validators.required,
      Validators.minLength(3)
    ]));

    this.form = this.fb.group({
      username: this.username,
      password: this.password
    });

    this.errorMsg$ = this.store.let(getAuthState()).select(s => s.error);

  }

  login(value:any) {
    this.store.dispatch(this.authActions.login({username: value.username, password: value.password}));
  }

}
