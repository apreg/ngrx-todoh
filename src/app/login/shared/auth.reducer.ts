import { Action } from '@ngrx/store';
import { User } from './user.model';
import { UserActions } from './user.action';
import {AuthActions} from './auth.action';
import { Observable } from 'rxjs/Observable';
import '@ngrx/core/add/operator/select';

export interface AuthState {
  error:string,
  currentUserId: string
};

const initialValue:AuthState = {
  error: '',
  currentUserId: ''
};

export default function(state:AuthState = initialValue, action:Action ):AuthState {
  switch (action.type) {
    case AuthActions.LOGIN_SUCCESS:
      return {error: '', currentUserId: action.payload.id};
    case AuthActions.LOGIN_FAILURE_WRONG_PASS:
      return {error: action.payload, currentUserId: ''};
    case AuthActions.LOGOUT_SUCCESS:
      return {error: '', currentUserId: ''};
    default:
      return state;
  }
};

export function getCurrentUserId() {
  return (state$: Observable<AuthState>) => state$.select(s => s.currentUserId);
}
