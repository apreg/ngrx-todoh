import { Injectable } from '@angular/core';
import { Effect, StateUpdates,StateUpdate, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';

import { AppState } from '../../shared/reducers';
import { AuthActions } from './auth.action';
import { User } from './user.model';
import {UserActions} from "./user.action";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/concat';


@Injectable()
export class UserEffects {
  constructor(private updates$:StateUpdates<AppState>, private authActions:AuthActions, private userActions:UserActions) {
  }

  @Effect() register$ = this.updates$
    .whenAction(UserActions.REGISTER_USER)
    .map(toPayload)
    .flatMap(p => p.username && p.password
      ? Observable.concat(Observable.of(this.userActions.registerUserSuccess(new User(p.username, p.password))), Observable.of(this.authActions.login(p)))
      : Observable.of(this.userActions.registerUserFailure("Invalid credentials")));

}
