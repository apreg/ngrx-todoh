import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { User } from './user.model';

@Injectable()
export class AuthActions {

  static LOGIN = '[AUTH] LOGIN';
  login(credentials:Object):Action {
    return {
      type: AuthActions.LOGIN,
      payload: credentials
    };
  }

  static LOGIN_SUCCESS = '[AUTH] LOGIN_SUCCESS';
  loginSuccessfull(user:User):Action {
    return {
      type: AuthActions.LOGIN_SUCCESS,
      payload: user
    };
  }

  static LOGIN_FAILURE_UNKNOWN_USER = '[AUTH] LOGIN_FAILURE_UNKNOWN_USER';
  loginFailUnknownUser(error):Action {
    return {
      type: AuthActions.LOGIN_FAILURE_UNKNOWN_USER,
      payload: error
    };
  }

  static LOGIN_FAILURE_WRONG_PASS = '[AUTH] LOGIN_FAILURE_WRONG_PASS';
  loginFailWrongPass(error):Action {
    return {
      type: AuthActions.LOGIN_FAILURE_WRONG_PASS,
      payload: error
    };
  }


  static LOGOUT = '[AUTH] LOGOUT';
  logout():Action {
    return {
      type: AuthActions.LOGOUT
    }
  }

  static LOGOUT_SUCCESS = '[AUTH] LOGOUT_SUCCESS';
  logoutSuccess():Action {
    return {
      type: AuthActions.LOGOUT_SUCCESS
    }
  }

  static IDLE_START = '[AUTH] IDLE_START';
  idleStart():Action {
    return {
      type: AuthActions.IDLE_START
    }
  }
}
