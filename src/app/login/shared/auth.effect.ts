import { Injectable } from '@angular/core';
import { Effect, StateUpdates,StateUpdate, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Router } from '@ngrx/router';

import { AppState } from '../../shared/reducers';
import { AuthActions } from './auth.action';
import { User } from './user.model';
import {UserActions} from './user.action';
import {Idle, DEFAULT_INTERRUPTSOURCES} from 'ng2-idle/core';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/switch';


@Injectable()
export class AuthEffects {
  constructor(private updates$:StateUpdates<AppState>, private authActions:AuthActions, private userActions:UserActions, private router:Router, private idle:Idle) {
  }

  // .map(query => console.log('query: ', query))
  // .switchMap(query => this.googleBooks.searchBooks(query)
  //   .map(books => this.bookActions.searchComplete(books))
  //   .catch(() => Observable.of(this.bookActions.searchComplete([])))
  // );
  /**
   * Effects offer a way to isolate and easily test side-effects within your
   * application. StateUpdates is an observable of the latest state and
   * dispatched action. The `toPayload` helper function returns just
   * the payload of the currently dispatched action, useful in
   * instances where the current state is not necessary.
   *
   * If you are unfamiliar with the operators being used in these examples, please
   * check out the sources below:
   *
   * Official Docs: http://reactivex.io/rxjs/manual/overview.html#categories-of-operators
   * RxJS 5 Operators By Example: https://gist.github.com/btroncone/d6cf141d6f2c00dc6b35
   */

  //TODO rewrite to pure stream based solution
  authLogic(update:StateUpdate<AppState>) {
    const incomingCredentials:any = update.action.payload;
    const users:User[] = update.state.users;
    let foundUser = users.find(u => u.username === incomingCredentials.username);
    if (foundUser) {
      if (foundUser.password === incomingCredentials.password) {

        this.router.go('/todo');
        return Observable.of(this.authActions.loginSuccessfull(foundUser));
      } else {
        return Observable.of(this.authActions.loginFailWrongPass("Invalid username/password"));
      }
    } else {
      return Observable.of(this.userActions.registerUser(incomingCredentials));
    }
  }

  @Effect() login$ = this.updates$
    .whenAction(AuthActions.LOGIN)
    //.map<User>(toPayload)
    .filter(update => update.action.payload.username !== '')
    .map(update => this.authLogic(update))
    .switch()
    ;


  @Effect() logout$ = this.updates$
    .whenAction(AuthActions.LOGOUT)
    .map(() => {
      this.router.go('/');
      return Observable.of(this.authActions.logoutSuccess())
    })
    .switch();
    //.filter(() => false);

}
