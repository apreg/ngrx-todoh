import util from '../../shared/util.ts';

export class User {
  id:string;
  username:string;
  password:string;
  todos:string[];// todoId

  constructor(username:string,
              password:string) {
    this.id = util.generateGuid();
    this.username = username;
    this.password = password;
    this.todos = [];
  }
}
