import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { User } from './user.model';

/**
 * Instead of passing around action string constants and manually recreating
 * action objects at the point of dispatch, we create services encapsulating
 * each appropriate action group. Action types are included as static
 * members and kept next to their action creator. This promotes a
 * uniform interface and single import for appropriate actions
 * within your application components.
 */
@Injectable()
export class UserActions {
  static REGISTER_USER:string = '[USER] REGISTER_USER';
  registerUser(userCredentials:Object):Action {
    return {
      type: UserActions.REGISTER_USER,
      payload: userCredentials
    };
  }

  static REGISTER_USER_SUCCESS = '[USER] REGISTER_USER_SUCCESS';
  registerUserSuccess(newUser:User):Action {
    return {
      type: UserActions.REGISTER_USER_SUCCESS,
      payload: newUser
    };
  }

  static REGISTER_USER_FAILURE = '[USER] REGISTER_USER_FAILURE';
  registerUserFailure(error:string):Action {
    return {
      type: UserActions.REGISTER_USER_FAILURE,
      payload: error
    };
  }
}
