import { Action } from '@ngrx/store';
import { User } from './user.model';
import { UserActions } from './user.action';
import { Observable } from 'rxjs/Observable';
import '@ngrx/core/add/operator/select';
import {TodoActions} from "../../todo/shared/todo.action";

export interface UserState extends Array<User> {};

const initialValue: UserState = [new User('asd', 'asd')];

export default (state: UserState = initialValue, action: Action = undefined): UserState => {
  switch (action.type) {
    case UserActions.REGISTER_USER_SUCCESS:
      return [...state, action.payload];
    default:
      return state;
  }
};

export function getUserById(userId: string) {
  return (state$: Observable<UserState>) => state$
    .select(s => s[name]);
}
