import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Todo } from './todo.model';


/**
 * Instead of passing around action string constants and manually recreating
 * action objects at the point of dispatch, we create services encapsulating
 * each appropriate action group. Action types are included as static
 * members and kept next to their action creator. This promotes a
 * uniform interface and single import for appropriate actions
 * within your application components.
 */
@Injectable()
export class TodoActions {
  static ADD_TODO:string = '[TODO] ADD_TODO';

  addTodo(todo:string, userId:string):Action {
    return {
      type: TodoActions.ADD_TODO,
      payload: {todo: todo, userId: userId}
    };
  }

  static REMOVE_TODO:string = '[TODO] REMOVE_TODO';

  removeTodo(id:string) {
    return {
      type: TodoActions.REMOVE_TODO,
      payload: id
    };
  }

  static TOGGLE_TODO:string = '[TODO] TOGGLE_TODO';

  toggleTodo(id:string) {
    return {
      type: TodoActions.TOGGLE_TODO,
      payload: id
    };
  }

  static LOAD_TODOS:string = '[TODO] LOAD_TODOS';

  loadTodos(userId:string) {
    return {
      type: TodoActions.LOAD_TODOS,
      payload: userId
    };
  }

  static LOAD_TODOS_SUCCESS:string = '[TODO] LOAD_TODOS_SUCCESS';

  loadTodosSuccess(todos:Todo[], userId: string) {
    return {
      type: TodoActions.LOAD_TODOS_SUCCESS,
      payload: {todos: todos, userId:userId}
    };
  }
}
