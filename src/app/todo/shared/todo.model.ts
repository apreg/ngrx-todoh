import util from '../../shared/util.ts';

export class Todo {
  id:string;
  completed:boolean;
  text:string;
  userId:string;

  constructor(title:string, userId: string) {
    this.id = util.generateGuid();
    this.completed = false;
    this.text = title;
    this.userId = userId;
  }
}
