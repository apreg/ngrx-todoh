import { Action } from '@ngrx/store';
import { Todo } from './todo.model';
import { TodoActions } from './todo.action';
import { Observable } from 'rxjs/Observable';

export interface TodoState extends Array<Todo> {};

const initialValue: TodoState = [];

// TODO switch parameters order, make action not initialized
export default function (state: TodoState = initialValue, action: Action = undefined): TodoState{
  switch (action.type) {
    case TodoActions.ADD_TODO:
      return [...state, new Todo(action.payload.todo, action.payload.userId)];

    case TodoActions.REMOVE_TODO:
      return state.filter((todo: Todo) => todo.id !== action.payload);

    case TodoActions.TOGGLE_TODO:
      return state.map((todo: Todo) => {
        if (todo.id === action.payload) {
          todo.completed = !todo.completed;
        }
        return todo;
      });

    default:
      return state;
  }
};

export function getTodosByUserId(userId: string) {
  // TODO check for userId not empty or something?
  return (state$: Observable<TodoState>) => state$.map((s:TodoState) => s.filter((x:Todo) => x.userId === userId));
}
