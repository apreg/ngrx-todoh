import {Component,Output, EventEmitter, Input, ChangeDetectionStrategy, SimpleChange} from '@angular/core'
import { Todo } from '../shared';

@Component({
  moduleId: module.id,
  selector: 'todo-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [require('./todo-item.component.css')],
  template: `<div class="todo-item"
     (swipeLeft)="remove.emit(todo.id)"
     (swipeRight)="toggle.emit(todo.id)"
     >

  <label class="text" [class.text--done]="completed">{{todo.text}}</label>
  <button
    class="toggle-container"
    (click)="onToggle()"
  > <img class="toggle-icon" [src]="imgSrc"/></button>
</div>`

})

export class TodoItemComponent {
  @Input() todo:Todo;
  @Input() completed:boolean;
  @Output() toggle:EventEmitter<string> = new EventEmitter();
  @Output() remove:EventEmitter<string> = new EventEmitter();
  imgSrc:string;

  constructor() {
    this.setImageSrc(this.completed);
  }

  ngOnChanges(changes:{[propertyName: string]: SimpleChange}) {
    if (changes.hasOwnProperty('completed')) {
      this.setImageSrc(changes['completed'].currentValue);
    }
  }

  onToggle():void {
    this.completed = !this.completed;
    this.toggle.emit(this.todo.id);
    this.setImageSrc(this.completed);
  }

  setImageSrc(completed:boolean) {
    this.imgSrc = completed ? 'assets/img/done.png' : 'assets/img/undone.png';
  }

}
