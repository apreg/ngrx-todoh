import {Component, EventEmitter, Input, Output, ChangeDetectionStrategy, OnInit } from '@angular/core';
import {ControlGroup, FormBuilder, Validators, Control, FORM_DIRECTIVES, CORE_DIRECTIVES} from "@angular/common";

@Component({
  moduleId: module.id,
  selector: 'todo-input',
  directives: [FORM_DIRECTIVES, CORE_DIRECTIVES],
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [require('./todo-input.component.css')],
  template: `<form [ngFormModel]="form" class="todo-input" (ngSubmit)="addTodo(form.value)">
  <input
    type="text"
    class="todo-input__input"
    autofocus
    placeholder="What to do...hmmmm"
    ngControl="todoText"
  />
  <button class="todo-input__button" [disabled]="!form.valid" type="submit"></button>
</form>
  `
})
export class TodoInputComponent implements OnInit {
  @Output() todo:EventEmitter<string> = new EventEmitter();
  form:ControlGroup;
  public todoText;

  constructor(private fb:FormBuilder) {
  }

  ngOnInit() {
    this.todoText = new Control('', Validators.compose([
      Validators.required,
      Validators.minLength(1)
    ]));
    this.form = this.fb.group({
      todoText: this.todoText,
    });

  }

  addTodo(todo):void {
    this.todo.emit(todo.todoText.trim());
    this.todoText.updateValue('');
  }
}
