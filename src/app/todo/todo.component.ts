import {Component, OnInit} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { AppState } from '../shared/reducers';
import { TodoState, Todo, TodoActions } from './shared';

import {TodoInputComponent} from './todo-input';
import {TodoItemsComponent} from "./todo-items";
import {AuthState} from "../login/shared/auth.reducer";
import { getCurrentUserId, getAuthState, getTodosOfCurrentUser } from '../shared/reducers';
import 'rxjs/add/operator/let';
import 'rxjs/add/operator/take';

@Component({
  selector: 'todo',
  directives: [TodoInputComponent, TodoItemsComponent],
  providers: [TodoActions],
  styles: [require('./todo.component.css')],
  template: `<div class="todo">
  <div class="logo-container">
  <img class="todo__logo" src="assets/img/todo-logo.png">
  </div>
  <todo-input (todo)="addTodo($event)"></todo-input>
  <todo-items
    [todos]="todos$ | async"
    (toggle)="onToggleTodo($event)"
    (remove)="onRemoveTodo($event)"
  ></todo-items>
</div>`
})
export class TodoComponent implements OnInit {
  todos$:Observable<TodoState>;

  constructor(private store:Store<AppState>, private todoActions:TodoActions) {
  }

  ngOnInit() {
    this.todos$ = this.store.let(getTodosOfCurrentUser());
  }

  addTodo(todo:string) {
    let currentUserId = null; //TODO handle
    this.store.let(getCurrentUserId()).subscribe(id =>currentUserId = id);
    this.store.dispatch(this.todoActions.addTodo(todo, currentUserId));
  }

  onToggleTodo(id:string) {
    this.store.dispatch(this.todoActions.toggleTodo(id));
  }

  onRemoveTodo(id:string) {
    this.store.dispatch(this.todoActions.removeTodo(id));
  }

}
