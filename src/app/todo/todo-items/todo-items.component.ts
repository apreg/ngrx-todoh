import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { TodoState } from '../shared/todo.reducer';
import { TodoItemComponent } from '../todo-item';
import { Todo } from '../shared/todo.model';

@Component({
  moduleId: module.id,
  selector: 'todo-items',
  directives: [TodoItemComponent],
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [require('./todo-items.component.css')],
  template: `
    <ul class="todo-items">
      <li class="wrapper" *ngFor="let todo of todos" >
        <todo-item
          [todo]="todo"
          [completed]="todo.completed"
          (toggle)="toggle.emit($event)"
          (remove)="remove.emit($event)"
          >
        </todo-item>
      </li>
    </ul>
  `
})
export class TodoItemsComponent {
  @Input() todos: TodoState;
  @Output() toggle: EventEmitter<string> = new EventEmitter();
  @Output() remove: EventEmitter<string> = new EventEmitter();

}
