/*
 * These are globally available services in any component or any other service
 */

// Angular 2
import { FORM_PROVIDERS } from '@angular/common';
// Angular 2 Http
import { HTTP_PROVIDERS } from '@angular/http';
// Angular 2 Router
import { EFFECT_PROVIDERS, ACTIONS_PROVIDERS } from '../app/shared';
import {default as STORE_PROVIDER } from '../app/shared/store.provider'; //TODO put it into index
import {default as ROUTES_PROVIDER} from '../app/shared/routes.provider';
import {IDLE_PROVIDERS} from 'ng2-idle/core'; // Import idle providers

/*
* Application Providers/Directives/Pipes
* providers/directives/pipes that only live in our browser environment
*/
export const APPLICATION_PROVIDERS = [
  ...FORM_PROVIDERS,
  ...HTTP_PROVIDERS,
  ...STORE_PROVIDER,
  ...ROUTES_PROVIDER,
  ...EFFECT_PROVIDERS,
  ...ACTIONS_PROVIDERS,
  IDLE_PROVIDERS,
];

export const PROVIDERS = [
  ...APPLICATION_PROVIDERS
];
