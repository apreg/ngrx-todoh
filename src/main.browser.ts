/*
 * Providers provided by Angular
 */
import { bootstrap } from '@angular/platform-browser-dynamic';
/*
 * Platform and Environment
 * our providers/directives/pipes
 */
import { PLATFORM_PROVIDERS } from './platform/browser';
import { ENV_PROVIDERS } from './platform/environment';

/*
 * App Component
 * our top level component that holds all of our components
 */
import { App } from './app';

/*
 * Bootstrap our Angular app with a top level component `App` and inject
 * our Services and Providers into Angular's dependency injection
 */
export function main() {

  return bootstrap(App, [
    ...PLATFORM_PROVIDERS,
    ...ENV_PROVIDERS
  ])
    .catch(err => console.error(err));

}

/*
 * Vendors
 * For vendors for example jQuery, Lodash, angular2-jwt just import them anywhere in your app
 * You can also import them in vendors to ensure that they are bundled in one file
 * Also see custom-typings.d.ts as you also need to do `typings install x` where `x` is your module
 */


/*
 * Hot Module Reload
 * experimental version by @gdi2290
 */
if ('development' === ENV) {
  main();
} else {
  // bootstrap when document is ready
  document.addEventListener('deviceready', () => {
    let model = device.model || "";
    if (model.indexOf("Samsung Galaxy S6") > -1 || model.indexOf("iPhone 5") > -1 || model.indexOf("iPhone 6") > -1) {
      main();
    }else {
      alert("Sorry, your device is not supported");
      navigator.app.exitApp();
    }

  }, false);
  //document.addEventListener('DOMContentLoaded', () => main());
}
